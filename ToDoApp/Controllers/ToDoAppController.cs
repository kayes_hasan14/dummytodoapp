﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoApp.Models;

namespace ToDoApp.Controllers
{
    public class ToDoAppController : Controller
    {
        private readonly ToDoDbContext _DbContext;
        
        public ToDoAppController(ToDoDbContext DbContext)
        {
            _DbContext = DbContext;
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ToDo todo)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }
            var rs = _DbContext.ToDos.Add(todo);
            _DbContext.SaveChanges();
            return RedirectToAction("GetAll");
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            List<ToDo> all = _DbContext.ToDos.ToList();
            return View(all);
        }

        [HttpGet]

        public IActionResult Edit(int id)
        {
            var data = _DbContext.ToDos.Find(id);
            
            return View(data);
        }

        [HttpPost]

        public IActionResult Edit(ToDo todo)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            
            var rs = _DbContext.ToDos.Find(todo.Id);
            rs.Name = todo.Name;
            rs.Description = todo.Description;
            rs.ScheduledTime = todo.ScheduledTime;
            _DbContext.SaveChanges();
            return RedirectToAction("GetAll");
        }

        [HttpGet]

        public IActionResult Delete(int id)
        {
            var data = _DbContext.ToDos.Find(id);
            _DbContext.ToDos.Remove(data);
            _DbContext.SaveChanges();

             
            return RedirectToAction("GetAll");
        }

        public IActionResult Details(int id)
        {
            var data = _DbContext.ToDos.Find(id);

            return View(data);
        }

       
    }
}
